import integrations
accelsX = []
accelsY = []
accelsZ = []
positsX = []
positsY = []
positsZ = []
axis = False
resetX = 0
resetY = 0
resetZ = 0


def acceleration(accelX, accelY, accelZ):
    """
    Get smoothed values from the sensor by sampling
    the sensor 5 times and returning the mean.
    """
    global accelsX
    global accelsY
    global accelsZ
    accelsX.append(accelX)
    accelsY.append(accelY)
    accelsZ.append(accelZ)

    if len(accelsX) == 5:
        # Add on value / samples (to generate an average)
        # with default of 0 for first loop.
        averageX = sum(accelsX) / len(accelsX)
        accelsX.clear()

        averageY = sum(accelsY) / len(accelsY)
        accelsY.clear()

        averageZ = sum(accelsZ) / len(accelsZ)
        accelsZ.clear()

        return averageX, averageY, averageZ
    else:
        return None, None, None


def position(positX, positY):
    """
    Smooth position values of an axis, so it is clear on which axis we're moving
    """
    global positsX
    global positsY
    global axis
    global resetX
    global resetY
    positsX.append(positX)
    positsY.append(positY)
    # Every five measurements a new axis is decided
    if len(positsX) == 5:
        # Calculating deltaX and deltaY in the 5 measurements
        deltaX = positsX[4] - positsX[0]
        deltaY = positsY[4] - positsY[0]
        lastX = positsX[4]
        lastY = positsY[4]
        positsX.clear()
        positsY.clear()
        changedAxis = axis  # To understand if there is a change in axis
        if deltaX > deltaY:
            axis = True  # The sensor is moving in the X axis
            if changedAxis != axis:  # If we changed axis we need to save the last Y position
                resetY = lastY
                # IMPORTANT! The integration of the position starts with the last saved X value
                integrations.positX = resetX
            return lastX, resetY
        else:
            axis = False  # The sensor is moving in the Y axis
            if changedAxis != axis:  # If we changed axis we need to save the last X position
                resetX = lastX
                # IMPORTANT! The integration of the position starts with the last saved X value
                integrations.positY = resetY
            return resetX, lastY
    # Otherwise next five measurements will be on previous decided axis
    else:
        if axis == True:
            return positX, resetY
        else:
            return resetX, positY

from machine import SD 
import pycom                                        # pycom board
from pysense import Pysense
from network import LoRa
import socket
import time
import math
import os
import ujson
import ubinascii
import binascii
import calculate
# Velocity and Displacement calculations
import integrations
# Acceleration smoothing algorithm
import smoothing
from MPL3115A2 import MPL3115A2, ALTITUDE, PRESSURE
from fusion import Fusion
from micropyGPS import MicropyGPS                   # GPS
# MicroPython I2C driver for MPU9250 9-axis motion tracking device
from mpu9250 import MPU9250
from ak8963 import AK8963                           # Magnetometer
from mpu6500 import MPU6500                         # Acc+Gyro
from machine import I2C, Pin, unique_id             # pins

pycom.heartbeat(False)  # LED

# SD Card
sd = SD()
os.mount(sd, '/sd')
f = open('/sd/test.txt', 'w')

# 1 g = 9.80665 m/s2 ie. standard gravity
SF_M_S2 = 9.80665

# IMU Stuff
i2c = I2C(1, pins=('P9', 'P10'))
buf1 = bytearray('\x00')

i2c.readfrom_mem_into(0x68, 0x75, buf1)
time.sleep(0.1)
i2c.writeto_mem(0x68, 0x6B, 0x01)                   # wake
time.sleep(0.1)
i2c.writeto_mem(0x68, 0x37, 0x02)                   # passthrough
# i2c.writeto_mem(0x68, 0x37, 0x01)

# imu = MPU9250(i2c)                                #IMU board
ak8963 = AK8963(i2c)  # magnetometer
mpu6500 = MPU6500(i2c)  # gyro+acc

# CALIBRATE MAGNETOMETER
print('Calibrating magnetometer')
offset, scale = ak8963.calibrate()
ak8963 = AK8963(i2c, offset=offset, scale=scale)
print('Magnetometer offset:', offset)
print('Magnetometer scale:', scale)

# CALIBRATE GYRO AND ACCELEROMETER
print('Calibrating accel and gyro')
offset_g, offset_a = mpu6500.calibrate()
mpu6500 = MPU6500(i2c, accel_sf=SF_M_S2, gyro_offset=offset_g, accelerometer_offset=offset_a)
print('accel offset_a:', offset_a)
print('gyro offset_g:', offset_g)
mpu6500 = MPU6500(i2c)

# ALTITUDE
py = Pysense()
# Returns height in meters
mp = MPL3115A2(py, mode=ALTITUDE)

# FUSION
fuse = Fusion()

deltat = 0.05  # 0.1
pycom.heartbeat(True)  # LED

while True:
    fuse.update(mpu6500.acceleration, mpu6500.gyro, ak8963.magnetic, mp.altitude())

    # accelerationX, accelerationY, accelerationZ = smoothing.acceleration(fuse.lin_x, fuse.lin_y, fuse.lin_z)
    accelerationX, accelerationY, accelerationZ = fuse.lin_x, fuse.lin_y, fuse.lin_z

    # Calculate distance vector
    positX, distanceX = calculate.distance(accelerationX, accelerationY, accelerationZ, deltat)
    # distanceX *= 10
    # positionX, positionY = smoothing.position(positX,positY)

    # Calculate X, Y, Z positions - Distance * heading angle. Update the coordinate with each function
    coordinateX, coordinateY, coordinateZ = integrations.getCoordinate()
    positionX = distanceX * math.cos(math.radians(fuse.heading)) + coordinateX
    positionY = distanceX * math.sin(math.radians(fuse.heading)) + coordinateY
    positionZ = distanceX * math.sin(math.radians(fuse.pitch)) + coordinateZ

    f = open('/sd/test.txt', 'a')
    f.write(str(positionX) + ',' + str(positionY) + ',' + str(positionZ) + '\n')
    # f.write(str(mpu6500.acceleration[0]) + ',' + str(mpu6500.acceleration[1]) + ',' + str(mpu6500.acceleration[2]) + ',' + str(mpu6500.gyro[0]) + ',' + str(mpu6500.gyro[1]) + ',' + str(mpu6500.gyro[2]) + ',' + str(ak8963.magnetic[0]) + ',' + str(ak8963.magnetic[1]) + ',' + str(ak8963.magnetic[2]) + ',' + str(fuse.pitch) + ',' + str(fuse.roll) + ',' + str(fuse.heading) + '\n')
    # f.write(str(mpu6500.acceleration[0]) + ',' + str(fuse.lin_x) + '\n')
    f.close()

    integrations.updateCoordinates(positionX, positionY, positionZ)

    # print('{},{},{},{}'.format(fuse.pitch, fuse.roll, fuse.heading, fuse.altitude))
    # print('{},{},{}'.format(fuse.pitch, fuse.roll, fuse.heading))
    # print('{},{},{}'.format(fuse.lin_x, fuse.lin_y, fuse.lin_z))
    # print('{},{}'.format(mpu6500.acceleration[0], fuse.lin_x))
    print('{},{},{}'.format(positionX, positionY, positionZ))
    time.sleep(deltat)

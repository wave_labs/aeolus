from squaternion import Quaternion
import numpy as np
from ahrs.filters import AngularRate, AQUA, Complementary, Davenport, EKF, FAMC, FLAE, Fourati, FQA, Madgwick, Mahony, OLEQ, QUEST, ROLEQ, SAAM, Tilt
import matplotlib.pyplot as plt
# import matplotlib.animation as animation
# from matplotlib.animation import FuncAnimation
# import matplotlib.patches as mpatches
import pandas as pd
import math
import integrations
import calculate

# Read data from files
df = pd.read_csv('marinedrone_field_experiment_7Oct2022.csv', )
a_time_stamp, acc, m_timesstamp, mag, g_timestamp,gyro,c_timestamp,compass,gp_timestam,coordinates, l_timestamp, illuminance, p_stamp, Pressure = df.iloc[:,0].values,df.iloc[:,1:4].values,df.iloc[:,4].values,df.iloc[:,5:8].values,df.iloc[:,8].values,df.iloc[:,9:12].values,df.iloc[:,12].values,df.iloc[:,13:16].values,df.iloc[:,16].values,df.iloc[:,17:20].values,df.iloc[:,20].values,df.iloc[:,21].values,df.iloc[:,22].values,df.iloc[:,23].values



# acc_data *= 9.8
# gyr_data, mag_data = np.hsplit(rest, [3])
# gyr_data *= 0.017453
# mag_data /= 1000
# Sampling rate
deltat = 0.01  # 10Hz

algorithms = [EKF, Madgwick, Mahony,FAMC, FLAE, FQA, OLEQ,QUEST,SAAM,Tilt,AQUA,Complementary,Davenport,Fourati, ROLEQ ]

for algorithm in algorithms:
    for frequency in range(100, 101):
        dist_data = []
        # Saving ahrs estimations to array
        estimation = []
        # Start ahrs estimation
        if algorithm == FAMC or algorithm == FLAE or algorithm == FQA or algorithm == OLEQ or algorithm == QUEST or algorithm == SAAM or algorithm == Tilt:
            ahrs = algorithm(acc=acc, mag=mag)  # Returns Quaternions
        else:
            ahrs = algorithm(acc=acc, gyr=gyro, mag=mag,
                             frequency=frequency)  # Returns Quaternions

        for t in range(1, acc.shape[0]):
            # Integrate distance from X-axis acceleration
            positX, distanceX,positY, distanceY,positZ, distanceZ = calculate.distance(
                acc[t-1][0], acc[t-1][1], acc[t-1][2], deltat)#ACC WAS DIVIDING BY 9.8
            dist_data.append([distanceX,distanceY,distanceZ])
            q = Quaternion(ahrs.Q[t][0], ahrs.Q[t][1],
                           ahrs.Q[t][2], ahrs.Q[t][3])
            estimation.append(q.to_euler(degrees=True))

        # Convert estimation to numpy array
        estimation = np.array(estimation)
        positions = []
        for index, distance in enumerate(dist_data, start=-1):
            # Calculate X, Y, Z positions - Distance * heading angle. Update the coordinate with each function
            coordinateX, coordinateY, coordinateZ = integrations.getCoordinate()
            distX, distY, distZ = distance
            positionX = distX * math.cos(math.radians(estimation[index][2])) + coordinateX
            positionY = distY * math.sin(math.radians(estimation[index][2])) + coordinateY
            positionZ = distZ * math.sin(math.radians(estimation[index][0])) + coordinateZ
            positions.append([positionX, positionY, positionZ])
            integrations.updateCoordinates(positionX, positionY, positionZ)

        integrations.updateCoordinates(0, 0, 0)
        x, y, z = [], [], []

        for position in positions:
            x.append(position[0])
            y.append(position[1])
            z.append(position[2])

            # PLOTS
        fig = plt.figure()
        ax = fig.add_subplot(projection='3d')
        ax.scatter(x, y, z)
        plt.savefig(f'{algorithm}.png')



        #
        # # PLOTS
        #
        # # 2D PLOTTING
        # plt.scatter(x, y)
        # plt.plot([0, 0, 4, 4, 0, 0, 8, 8, 0, 0, 12, 12, 0, 0, 16, 16, 0, 0, 20, 20, 0, 0, 24, 24, 0, 0, 28, 28, 0], [
        #          0, 4, 4, 0, 0, 8, 8, 0, 0, 12, 12, 0, 0, 16, 16, 0, 0, 20, 20, 0, 0, 24, 24, 0, 0, 28, 28, 0, 0])
        # plt.suptitle('Madgwick AHRS')
        # # plt.show()
        # plt.savefig('figuresMadgwick/figure' + str(frequency))
        # plt.clf()
        #
        # # # 3D PLOTTING
        # # fig = plt.figure()
        # # plt.plot([0, 0, 4, 4, 0, 0, 8, 8, 0, 0, 12, 12, 0, 0, 16, 16, 0, 0, 20, 20, 0, 0, 24, 24, 0, 0, 28, 28, 0],
        # #          [0, 4, 4, 0, 0, 8, 8, 0, 0, 12, 12, 0, 0, 16, 16,
        # #              0, 0, 20, 20, 0, 0, 24, 24, 0, 0, 28, 28, 0, 0],
        # #          [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        # # ax = fig.add_subplot(111, projection='3d')
        # # ax.scatter(x, y, z)
        # # plt.show()
        # # plt.savefig('figuresMadgwick/figure' + str(frequency))
        #
        # # VIDEO PLOTTING
        # # fig = plt.figure()
        # # # creating a subplot
        # # ax1 = fig.add_subplot(1, 1, 1)
        #
        # # xs = []
        # # ys = []
        #
        # # def animate(i):
        # #     xs.append(x[i])
        # #     ys.append(y[i])
        #
        # #     ax1.clear()
        # #     ax1.plot(xs, ys)
        #
        # # ani = animation.FuncAnimation(fig, animate, interval=1, frames=5000)
        #
        # # plt.show()
        # # ani.save('2Dplot.gif')

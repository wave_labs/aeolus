import numpy as np
from ahrs.filters import Madgwick
import matplotlib.pyplot as plt
from squaternion import Quaternion 
import integrations
import calculate
import math

# Read data from 'data.txt'. Files should not have commas or indexes. Only plain data.
data = np.loadtxt('data.txt')

# Split first 3 acceleration columns
acc_data, rest = np.hsplit(data, [3])

# G to ms^-2
acc_data *= 9.8

# Split first 3 gyroscope columns
gyr_data, mag_data = np.hsplit(rest,[3])

# deg/s to rad/s
gyr_data *= 0.017453

# Gauss to mT
mag_data /= 10

# Sampling rate
deltat = 0.1 # 10Hz

# Start Madgwick estimation
madgwick = Madgwick(acc=acc_data, gyr=gyr_data, mag=mag_data, frequency=10) # Returns Quaternions

# Saving madgwick estimations to array
estimation = []

# Array with integrated distances
dist_data = []

# For every row of data
for t in range(1, len(acc_data)):
    # Integrate distance from X-axis acceleration 
    positX, distanceX = calculate.distance(acc_data[t][0], acc_data[t][1], acc_data[t][2], deltat)
    dist_data.append(distanceX)
    q = Quaternion(madgwick.Q[t][0],madgwick.Q[t][1],madgwick.Q[t][2],madgwick.Q[t][3])
    # Convert estimated quaternion AHRS to euler angles 
    estimation.append(q.to_euler(degrees=True))

# Convert estimation to numpy array 
estimation = np.array(estimation)

positions = []

for index, distance in enumerate(dist_data,start=-1) :
    # Calculate X, Y, Z positions - Distance * heading angle. Update the coordinate with each function
    coordinateX, coordinateY, coordinateZ = integrations.getCoordinate()
    positionX = distance * math.cos(math.radians(estimation[index][2])) + coordinateX
    positionY = distance * math.sin(math.radians(estimation[index][2])) + coordinateY
    positionZ = distance * math.sin(math.radians(estimation[index][0])) + coordinateZ
    positions.append([positionX,positionY,positionZ])
    integrations.updateCoordinates(positionX, positionY, positionZ)


x, y, z = [], [], []

for position in positions:
    x.append(position[0])
    y.append(position[1])
    z.append(position[2])

fig = plt.figure()
ax = fig.add_subplot(projection='3d')
ax.scatter(x, y, z)
plt.show()
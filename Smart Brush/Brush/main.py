from pyquaternion import Quaternion
import numpy as np
import matplotlib.pyplot as plt
from def1 import quaternion_rotation, compute_translation_local, dt, cumulative_sum_vector

for j in range(1,4):

    # Load the CSV file into a numpy array
    data = np.loadtxt(f'BrushAcc_{j}Trial.csv', delimiter=',',usecols=(0,1,2,5,7,9))

    accel = np.zeros(data[:,3:6].shape)
    average_accel = np.zeros((1,3))
    for i in range(data.shape[0]):
        accel[i,:]=[data[i,3]/1000*9.8,data[i,4]/1000*9.8,data[i,5]/1000*9.8]



    # Extract the second, third, and fourth columns into a new array


    velocity_local = [0, 0, 0]
    orientation = Quaternion(1, 0, 0, 0)

    position = np.zeros((1, 3))
    position[0] = np.array([0, 0, 0])

    sum_accel = np.array([0,0,0])
    sum_time = 0

    sample_size = 11#variable is used to specify the number of readings that are averaged together to reduce noise in the accelerometer data.
    for i in range(data.shape[0]-1):
        delta_t = 1/11

        if i % sample_size != 0 and i != 0:
            sum_accel = cumulative_sum_vector(sum_accel, accel[i])
            sum_time += delta_t

        else:

            displacement, velocity_local = compute_translation_local(sum_accel / sample_size, sum_time,
                                                                     velocity_local)
            new_position = [position[-1][j] + displacement[j] for j in range(3)]
            average_accel = np.vstack((average_accel,sum_accel/11))
            position = np.vstack([position,new_position])
            sum_accel = accel[i]
            sum_time = delta_t

        # Store position for plotting or further analysis
    x1, y1, z1 = [], [], []
    for posit in position:
        x1.append(posit[0])
        y1.append(posit[1])
        z1.append(posit[2])
    time = np.arange(len(average_accel[:,0])) * 1 / 11
    plt.plot(time, average_accel[:,:])




    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    ax.scatter(x1, y1, z1)
    plt.savefig(f"Brush{j}.png")